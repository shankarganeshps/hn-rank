# hackernews seems to max out on page 24
for page in `seq 1 25`; 
do
	curl -D - "https://news.ycombinator.com/news?p=$page" | grep -i $1
	if [ $? -ne 1 ]; then
		break
	fi
	echo "----------------------------------------------------------------------"	
	echo "Not on page $page"
	echo "----------------------------------------------------------------------"
done
echo "----------------------------------------------------------------------"
echo "Found on page $page: https://news.ycombinator.com/news?p=$page"
echo "----------------------------------------------------------------------"
